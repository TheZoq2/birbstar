# top=correlator::accumulators

from spade import *
from cocotb.clock import Clock
# TODO: Migrate into prelude
from cocotb.triggers import FallingEdge

@cocotb.test()
async def accumulator_test(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units="ns").start())

    s.i.rst = "true"
    [await FallingEdge(clk) for _ in range(0, 5)]
    s.i.rst = "false"
    s.i.new_value = "None()"

    # Initialise the accumulators
    for i in range(0, 40):
        s.i.clear_command = f"Some({i})"
        await FallingEdge(clk)
    s.i.clear_command = "None()"

    # Check the buffers to ensure they are cleared
    s.i.read_index = "0"
    await FallingEdge(clk);
    s.o.read_result.assert_eq("0")

    s.o.max.assert_eq("None()")

    s.i.read_index = "10"
    await FallingEdge(clk);
    s.o.read_result.assert_eq("0")

    # TODO: Allow assignment of straight integers when applicable
    s.i.read_index = "39"
    await FallingEdge(clk);
    s.o.read_result.assert_eq("0")

    # Try to accumulate something
    s.i.new_value = "Some((0, 1))"
    await FallingEdge(clk);
    s.i.new_value = "None()"

    # Wait for acccumulation
    s.i.read_index = "0"
    [await FallingEdge(clk) for _ in range(0, 3)]
    s.o.read_result.assert_eq("1")

    # Accumulate 2 into index 0, then 1 into index 1
    s.i.new_value = "Some((0, 2))"
    await FallingEdge(clk)
    s.i.new_value = "Some((1, 1))"
    await FallingEdge(clk)
    s.i.new_value = "None()"

    s.i.read_index = "0"
    await FallingEdge(clk)
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.o.read_result.assert_eq("3")
    s.i.read_index = "1"
    await FallingEdge(clk)
    s.o.read_result.assert_eq("1")

    s.o.max.assert_eq("Some((0, 3))")

