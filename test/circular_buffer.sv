`include "test/vatch/main.v"

module circular_buffer_tb();
    `SETUP_TEST
    // 5 should flush the whole pipeline before doing anything
    `CLK_AND_RST(clk, rst, 5)

    reg[7:0] read_addr;
    reg[1+10-1:0] write;

    wire[9:0] result;

    `define NO_WRITE write = {1'b1, 10'dx};
    `define WRITE(val) write = {1'b0, val};

    initial begin
        `NO_WRITE;

        @(negedge rst)
        `WRITE(10'd1);

        @(negedge clk);
        `NO_WRITE;

        // Attempt to read the newly written sample.
        // Buffer is WAR, so we need an extra cycle between here
        read_addr = 0; 
        @(negedge clk);
        `ASSERT_EQ(result, 1);

        // Write another sample
        `WRITE(10'd2);
        @(negedge clk);
        `NO_WRITE;
        read_addr = 0;
        @(negedge clk)

        // The newly written sample is now at address 0
        `ASSERT_EQ(result, 2);
        read_addr = 1;
        @(negedge clk)
        // And the old sample is at address 1
        `ASSERT_EQ(result, 1);

        // Write 6 more values
        `WRITE(10'd3); @(negedge clk);
        `WRITE(10'd4); @(negedge clk);
        `WRITE(10'd5); @(negedge clk);
        `WRITE(10'd6); @(negedge clk);
        `WRITE(10'd7); @(negedge clk);
        `WRITE(10'd8); @(negedge clk);
        `NO_WRITE;

        // Wait for write to occur
        @(negedge clk);

        // Read the samples
        read_addr <= 0; @(negedge clk); `ASSERT_EQ(result, 8);
        read_addr <= 1; @(negedge clk); `ASSERT_EQ(result, 7);
        read_addr <= 2; @(negedge clk); `ASSERT_EQ(result, 6);
        read_addr <= 3; @(negedge clk); `ASSERT_EQ(result, 5);
        read_addr <= 4; @(negedge clk); `ASSERT_EQ(result, 4);
        read_addr <= 5; @(negedge clk); `ASSERT_EQ(result, 3);
        read_addr <= 6; @(negedge clk); `ASSERT_EQ(result, 2);
        read_addr <= 7; @(negedge clk); `ASSERT_EQ(result, 1);

        #10
        `END_TEST
    end

    e_proj_correlator_circular_buffer uut
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_size(8'd8)
        , ._i_read(read_addr)
        , ._i_write(write)
        , .__output(result)
        );
endmodule
