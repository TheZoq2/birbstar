`include "test/vatch/main.v"

module hex_to_ascii_tb();
    `SETUP_TEST
    // 5 should flush the whole pipeline before doing anything
    `CLK_AND_RST(clk, rst, 5)

    reg[3:0] hex;
    reg[7:0] o;

    initial begin
        hex = 0;
        #1
        `ASSERT_EQ(o, 48);

        hex = 9;
        #1
        `ASSERT_EQ(o, 57);

        hex = 10;
        #1
        `ASSERT_EQ(o, 65);

        hex = 15;
        #1
        `ASSERT_EQ(o, 70);
        #1

        `END_TEST
    end

    e_proj_main_hex_to_ascii uut(
        ._i_hex(hex),
        .__output(o)
    );
endmodule
