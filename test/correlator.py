# top=correlator::correlator
from spade import *

from cocotb.clock import Clock
from cocotb.triggers import ClockCycles
from cocotb.triggers import FallingEdge

async def insert_sample(clk, buffer_size, s, val):
    s.i.new_samples = f"Some({val})"
    await FallingEdge(clk)
    s.i.new_samples = "None()"
    await ClockCycles(clk, buffer_size, rising=False)

@cocotb.test()
async def simple_correlation_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units="ns").start())

    buffer_size = 128;

    s.i.new_samples = "None()"

    s.i.clear = "true"
    s.i.rst = "true"
    await ClockCycles(clk, 10, rising=False)
    s.i.rst = "false"

    # Wait for clear
    await ClockCycles(clk, buffer_size, rising=False)
    s.i.clear = "false"

    for i in range(0, buffer_size):
        s.i.read_index = f"{i}"
        # 6 is the pipeline depth.
        await ClockCycles(clk, 6, rising=False)
        s.o.read_result.assert_eq("0")

    await insert_sample(clk, buffer_size, s, "(1, 1)")

    # Insert an additional `buffer_size` samples to make the buffers
    # "catch up"
    for i in range(0, buffer_size):
        await insert_sample(clk, buffer_size, s, "(0, 0)")


    # Wait for correlation to finish
    await ClockCycles(clk, buffer_size);

    s.i.read_index = f"{buffer_size // 2 - 1}";
    await ClockCycles(clk, 6, rising=False)
    s.o.read_result.assert_eq("0")

    s.i.read_index = f"{buffer_size // 2}";
    await ClockCycles(clk, 6, rising=False)
    s.o.read_result.assert_eq("1")


    s.i.read_index = f"{buffer_size // 2 + 1}";
    await ClockCycles(clk, 6, rising=False)
    s.o.read_result.assert_eq("0")

@cocotb.test()
async def correlation_with_negative_offset_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 1, units="ns").start())

    buffer_size = 128;

    s.i.new_samples = "None()"

    s.i.clear = "true"
    s.i.rst = "true"
    await ClockCycles(clk, 10, rising=False)
    s.i.rst = "false"

    # Wait for clear
    await ClockCycles(clk, buffer_size, rising=False)
    s.i.clear = "false"

    for i in range(0, buffer_size):
        s.i.read_index = f"{i}"
        # 6 is the pipeline depth.
        await ClockCycles(clk, 6, rising=False)
        s.o.read_result.assert_eq("0")

    await insert_sample(clk, buffer_size, s, "(0, 1)");
    await insert_sample(clk, buffer_size, s, "(0, 0)");
    await insert_sample(clk, buffer_size, s, "(1, 0)");

    # Insert an additional `buffer_size` samples to make the buffers
    # "catch up"
    for i in range(0, buffer_size):
        await insert_sample(clk, buffer_size, s, "(0, 0)")


    # Wait for correlation to finish
    await ClockCycles(clk, buffer_size);

    s.i.read_index = f"{buffer_size // 2 - 2}";
    await ClockCycles(clk, 6, rising=False)
    s.o.read_result.assert_eq("0")

    s.i.read_index = f"{buffer_size // 2}";
    await ClockCycles(clk, 6, rising=False)
    s.o.read_result.assert_eq("0")


    s.i.read_index = f"{buffer_size // 2 + 2}";
    await ClockCycles(clk, 6, rising=False)
    s.o.read_result.assert_eq("1")
