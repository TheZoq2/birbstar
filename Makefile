PROJECT_NAME = s1_blinky_demo
S1_SDK_PATH = s1-sdk

# Put the nRF SDK path here. If you don't have it, download it here:
# https://www.nordicsemi.com/Products/Development-software/nRF5-SDK
NRF_SDK_PATH ?= /opt/nrf5-sdk

# Put your arm GCC path here. If you don't have it, download it here:
# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
GNU_INSTALL_ROOT ?= /usr/bin/
GNU_PREFIX ?= arm-none-eabi

# Source files
SRC_FILES += \
  firmware/main.c \

# Include paths
INC_FOLDERS += \
  firmware \
  build \

OUTPUT_DIRECTORY=build

# This is where the magic happens.
include s1-sdk/Makefile
