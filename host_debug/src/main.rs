use std::env;
use std::io;
use std::path::PathBuf;
use std::time::Duration;
use std::io::prelude::*;

use serial::prelude::*;
use clap::Parser;

#[derive(Parser)]
struct Args {
    port: PathBuf
}

fn main() {
    let args = Args::parse();

    for arg in env::args_os().skip(1) {
        let mut port = serial::open(&arg).unwrap();
        read_5_sec(&mut port).unwrap();
    }
}

fn read_5_sec<T: SerialPort>(port: &mut T) -> io::Result<()> {
    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::BaudOther(230400))?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    })?;

    port.set_timeout(Duration::from_millis(1000))?;

    let mut read_count = 0;

    let mut bytes = vec![];
    loop {
        let mut buf = [0;1024];

        println!("{read_count}");

        // port.write(&buf[..])?;
        let new_count = port.read(&mut buf[..])?;
        read_count += new_count;

        bytes.extend_from_slice(&buf[0..new_count]);

        if read_count > 22050 * 5 {
            break
        }
    }

    std::fs::write("out.raw", bytes)?;

    Ok(())
}
