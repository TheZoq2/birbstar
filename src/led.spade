use ws2812::main::state_gen;
use ws2812::main::output_gen;
use ws2812::main::OutputControl;
use ws2812::main::Timing;
use ws2812::main::Color;

use std::ops::div_pow2;

pipeline(1) led_display(
    clk: clock,
    rst: bool,
    correlation_max: Option<(int<10>, int<40>)>
) -> bool {
        // Target 12 Mhz
        let t = Timing$(
            us280: 8720,
            us0_4: 9,
            us0_8: 19,
            us0_45: 10,
            us0_85: 20,
            us1_25: 30,
        );
        // *NOTE* Changed to 1 led for testing
        let _16: int<6> = 16;
        let ctrl: OutputControl<int<5>> = inst state_gen(clk, rst, trunc(_16), t);

        let brightness = 16;

        // Prevent color changes of LEDs while emitting
        reg(clk) max_latch = match ctrl {
            OutputControl::Ret => correlation_max,
            _ => max_latch
        };
    reg;
        let with_color = match ctrl {
            OutputControl::Ret => OutputControl::Ret(),
            OutputControl::Led$(payload: led_num, bit, duration) => {
                // let color = if led_num == 0 {
                //     Color(brightness, 0, 0)
                // } else if led_num == 1 {
                //     Color(0, brightness, 0)
                // } else {
                //     Color(0, 0, brightness)
                // };

                // OutputControl::Led$(payload: color, bit, duration)

                match max_latch {
                    Some((max, _)) => {
                        let target_led = ((max - 64) `div_pow2` 1) + 7;
                        let color = if target_led > 0 && target_led < 16 && trunc(target_led) == led_num {
                            Color(brightness, brightness, brightness)
                        }
                        else {
                            Color(0, 0, 0)
                        };
                        OutputControl::Led$(payload: color, bit, duration)
                    },
                    None => OutputControl::Led$(payload: Color(0,0,0), bit, duration)
                }
            },
        };

        output_gen(with_color, t)
}
