use std::ops::div_pow2;

use mcp3002::adc;

use lib::display::display_driver;
use lib::display::DisplayPins;
use lib::display::WaveformDisplayOut;
use lib::correlator::circular_buffer_pipe;

struct Result {
    adc1_pins: mcp3002::adc::AdcPins,
    adc2_pins: mcp3002::adc::AdcPins,
    led_pin: bool,
    debug_pin: bool
}

enum DebugState {
    WaitUart,
    WaitAdc,
}

fn hex_to_ascii(hex: int<4>) -> int<8> {
    // 63
    let hex_: int<8> = zext(hex);
    if hex_ < 10 {
        let resulta = hex_ + 48;
        trunc(resulta)
    }
    else {
        let resultb = hex_ + 55;
        trunc(resultb)
    }
    // else {
    //     63
    // }
}


entity main(clk: clock, rst: bool, miso1_unsync: bool, miso2_unsync: bool) -> Result {
    reg(clk) miso1 = miso1_unsync;
    reg(clk) miso2 = miso2_unsync;

    // let hz_44100 = 363;
    // let second_0_1 = 16_00_000;
    // let second_1 = 16_000_000;

    let hz_44100 = 545;
    // let second_0_1 = 24_00_000;
    let second_1 = 24_000_000;

    reg(clk) sample_counter: int<32> reset (rst: 0) = {
        if sample_counter == hz_44100 {
            0
        }
        else {
            trunc(sample_counter + 1)
        }
    };
    let next_sample = sample_counter == 0;

    let adcs = (
        inst adc::adc_driver$(
            clk,
            rst,
            read_cmd: if next_sample {Some(~0)} else {None()},
            miso: miso1,
            timing: adc::timing_24mhz()
        ),
        inst adc::adc_driver$(
            clk,
            rst,
            read_cmd: if next_sample {Some(~0)} else {None()},
            miso: miso2,
            timing: adc::timing_24mhz()
        )
    );

    // decl samp_read_addr;

    // reg(clk) slow_timer: int<32> reset(rst: 0) = if slow_timer == second_0_1 {
    //     0
    // }
    // else {
    //     trunc(slow_timer + 1)
    // };

    // reg(clk) read_buff0 reset(rst: true) = if slow_timer == 0 {
    //     !read_buff0
    // } else {
    //     read_buff0
    // };

    // let write_samples = (adcs#0.result, adcs#1.result);
    // let samp_mem_out = [
    //     inst(1) sample_memory(clk, rst, samp_read_addr, write_samples, !read_buff0),
    //     inst(1) sample_memory(clk, rst, samp_read_addr, write_samples, read_buff0),
    // ];

    let new_samples = match (adcs#0.result, adcs#1.result) {
        (Some(sl), Some(sr)) => Some((trunc(sl), trunc(sr))),
        _ => None(),
    };

    reg(clk) slow_timer2: int<32> reset(rst: 0) = if slow_timer2 == second_1 {
        0
    }
    else {
        trunc(slow_timer2 + 1)
    };

    let correlation_result = inst(7) lib::correlator::correlator(
        clk,
        rst,
        new_samples,
        0, // samp_read_addr `div_pow2` 1,
        slow_timer2 == 0
    );


    let led_out = inst(1) lib::led::led_display(clk, rst, correlation_result.max);
    // let led_out = inst lib::led::led_display(clk, rst, Some((64, 1)));

    reg(clk) timer: int<32> reset(rst: 0) = if timer > 24_000_000 {
        0
    }
    else {
        trunc(timer+1)
    };

    Result$(
        // adc1_pins: mcp3002::adc::AdcPins(false, false, false),
        // adc2_pins: mcp3002::adc::AdcPins(false, false, false),
        adc1_pins: adcs#0.pins,
        adc2_pins: adcs#1.pins,
        led_pin: led_out,
        debug_pin: false // timer < 12_000_000,
    )
}

pipeline(1) sample_memory(
    clk: clock,
    rst: bool,
    read_addr: int<10>,
    write_data: (Option<int<16>>, Option<int<16>>),
    write_enable: bool,
) -> (int<10>, int<10>) {
        let o = (
            inst(1) circular_buffer_pipe(
                clk,
                rst,
                400,
                read_addr,
                if write_enable {
                    match write_data#0 {Some(v) => Some(trunc(v)), None => None()}
                }
                else {
                    None()
                }
            ),
            inst(1) circular_buffer_pipe(
                clk,
                rst,
                400,
                read_addr,
                if write_enable {
                    match write_data#1 {Some(v) => Some(trunc(v)), None => None()}
                }
                else {
                    None()
                }
            ),
        );
    reg;
        o
}



