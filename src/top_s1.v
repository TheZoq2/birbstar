module top (
            input wire miso1,
            input wire miso2,
            output wire debug_led,
            output wire mosi,
            output wire sclk,
            output wire cs,
            output wire led_pin
           );


    wire dummy_mosi;
    wire dummy_sclk;
    wire dummy_cs;

    wire clk;

    // Configure internal HS oscillator as 24MHz
	SB_HFOSC #(.CLKHF_DIV("0b01"))  osc(.CLKHFEN(1'b1),     // enable
  										.CLKHFPU(1'b1), // power up
  										.CLKHF(clk)         // output to sysclk
                                        ) /* synthesis ROUTE_THROUGH_FABRIC=0 */;


    // wire rst;
    // reg [3:0] rststate = 0;
    // assign rst = &rststate;
    // always @(posedge clk) rststate <= rststate + !rst;

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end


    \birbstar::main::main  uut
        ( .clk_i(clk)
        , .rst_i(rst)
        , .miso1_unsync_i(miso1)
        , .miso2_unsync_i(miso2)
        , .output__({
            sclk,
            mosi,
            cs,

            dummy_sclk,
            dummy_mosi,
            dummy_cs,

            led_pin,
            debug_led
        })
        );

    // reg one = 1;
    // reg [20:0] counter;

    // always @(posedge clk) begin
    //     // Count and increment the output port
    //     counter <= counter + 1'b1;
    //     if(counter == 20'd600000) begin
    //         counter <= 0;
    //     end
    // end

    // assign debug_led = one;

    // assign D3 = port[2]; // LED should toggle every 400mS
endmodule

