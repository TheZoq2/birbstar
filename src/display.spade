use memory_display::main::OutputPins as DisplayPins;
use memory_display::main::Output;
use memory_display::main::Timing as DisplayTiming;
use memory_display::main::display_gen;
use memory_display::main::output_translator;

use std::ops::div_pow2;
use std::ops::abs_trunc;

struct WaveformDisplayOut {
    pins: DisplayPins,
    read_address: int<10>,
}

fn sample_graph(
    x: int<10>,
    y: int<10>,
    sample: int<10>,
    scale: int<10>,
    dash: bool
) -> bool
{
    let y = trunc(y - 120);
    let s = (sample `div_pow2` scale);
    (y == s || trunc(y+1) == s) && (!dash || ((x `div_pow2` 1) & 1) == 1)
}

fn correlation_graph(
    x: int<10>,
    y: int<10>,
    correlation_result: int<40>
) -> bool {
    let c = trunc((-abs_trunc(correlation_result) `div_pow2` 15) + 190);
    y == c || trunc(y+1) == c
}

fn vertical_line(
    x: int<10>,
    y: int<10>,
    pos: int<10>,
    dash: bool
) -> bool {
    (x == pos || trunc(x+1) == pos) && (!dash || (((y `div_pow2` 1) & 1) == 1))
}

fn best_match(x: int<10>, y: int<10>, correlation_max: Option<(int<10>, int<40>)>)
    -> bool
{
    match correlation_max {
        Some((offset, val)) => vertical_line(x, y, offset << 1, false),
        None => false,
    }
}

pipeline(2) display_driver(
    clk: clock,
    rst: bool,
    // The samples read from the 2 buffers containing ADC signals
    samples_: (int<10>, int<10>),
    correlation_result: int<40>,
    correlation_max: Option<(int<10>, int<40>)>
) -> WaveformDisplayOut {
        'initial_stage
        let disp_timing = DisplayTiming$(
            us1: 16,
            us3: 48,
            mhz1: 8
        );

        let display_signals = inst display_gen(clk, rst, disp_timing);
    reg;
        'address_gen
        let read_address = match display_signals.o {
            Output::UpdateMode => 0,
            Output::FrameInv(v) => 0,
            Output::AllClear => 0,
            Output::Dummy => 0,
            Output::Address(v) => 0,
            Output::Pixel((x, y)) => x,
            Output::CsHigh => 0,
            Output::CsLow => 0,
        };
    reg;
        let samples = stage(initial_stage).samples_;
        let with_pixels = match display_signals.o {
            Output::UpdateMode => Output::UpdateMode(),
            Output::FrameInv(v) => Output::FrameInv(v),
            Output::AllClear => Output::AllClear(),
            Output::Dummy => Output::Dummy(),
            Output::Address(v) => Output::Address(v),
            Output::Pixel((x, y)) => {
                let scale = 1;
                let y = zext(y);
                Output::Pixel(!(
                    sample_graph(x, y, samples#0, scale, true) ||
                    sample_graph(x, y, samples#1, scale, false) ||
                    correlation_graph$(x, y, correlation_result) ||
                    vertical_line(x, y, 128, true) ||
                    best_match(x, y, correlation_max)
                ))
            },
            Output::CsHigh => Output::CsHigh(),
            Output::CsLow => Output::CsLow(),
        };
        WaveformDisplayOut$(
            pins: output_translator(with_pixels, display_signals.sclk),
            read_address: stage(address_gen).read_address,
        )
}
