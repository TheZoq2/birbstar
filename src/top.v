module top
    ( input clk
    , input rx
    , input adc1_miso
    , input adc2_miso
    , output adc1_mosi
    , output adc1_sclk
    , output adc1_cs
    , output adc2_mosi
    , output adc2_sclk
    , output adc2_cs
    , output display_cs
    , output display_sclk
    , output display_mosi
    , output led_pin
    );

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    \birbstar::main::main main
        ( .clk_i(clk)
        , .rst_i(rst)
        , .rx_unsync_i(rx)
        , .miso1_unsync_i(adc1_miso)
        , .miso2_unsync_i(adc2_miso)
        , .output__({
            adc1_sclk,
            adc1_mosi,
            adc1_cs,
            adc2_sclk,
            adc2_mosi,
            adc2_cs,
            display_sclk,
            display_cs,
            display_mosi,
            led_pin
        })
        );
endmodule
